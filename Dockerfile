ARG BASE_IMAGE=public.ecr.aws/ews-network/python:3.9
FROM $BASE_IMAGE as builder

WORKDIR /temp
COPY pyproject.toml /temp/
COPY poetry.lock /temp/
RUN pip install pip poetry -U
RUN poetry export --without-hashes > requirements.txt


FROM $BASE_IMAGE

RUN yum install shadow-utils -y && \
    groupadd -r app -g 1042 && useradd -u 1042 -r -g app -m -d /app -s /sbin/nologin -c "App user" app && chmod 755 /app && \
    yum erase shadow-utils -y; yum install curl -y; yum clean all && \
    (pip --version && pip install pip -U --no-cache-dir) \
      || curl -sS https://bootstrap.pypa.io/get-pip.py | python /opt/get-pip.py

WORKDIR /app
USER app
COPY --from=builder /temp/requirements.txt requirements.txt

ENV PATH /app/.local/bin:$PATH
RUN pip install --no-cache-dir -r requirements.txt --user

COPY --chown=app:app schema_registry_proxy schema_registry_proxy
COPY --chown=app:app app.py app.py
COPY --chown=app:app entrypoint.sh entrypoint.sh


ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["flask", "run"]
