# SPDX-License-Identifier: Apache-2.0
# Copyright 2020-2022 John Mille <john@compose-x.io>

from botocore.client import ClientError
from boto3.session import Session
from compose_x_common.compose_x_common import keyisset
import json


class Secret:
    def __init__(self, secret_id: str, session: Session = None):
        self.session = session if session else Session()
        self._name = secret_id
        self._retrieved_at = None

    @property
    def secret_string(self):
        client = self.session.client("secretsmanager")
        try:
            value = client.get_secret_value(
                SecretId=self.name, VersionStage="AWSCURRENT"
            )
            if keyisset("SecretString", value):
                return value["SecretString"]
            raise KeyError("No SecretString in reply from SecretsManager")
        except (client.exceptions.ResourceNotFoundException, ClientError):
            raise

    @property
    def secret_json(self) -> dict:
        try:
            return json.loads(self.secret_string)
        except json.JSONDecodeError:
            return self.secret_string

    @property
    def name(self) -> str:
        return self._name
