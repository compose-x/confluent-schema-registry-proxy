"""
Flask
"""

from tempfile import TemporaryDirectory

import json
import requests
from flask import Flask, jsonify
from werkzeug.exceptions import HTTPException

from .schema import Schema
from .config import Config

APP = Flask(__name__)
CFG = Config()
APP.config.from_object(CFG)
registry = APP.config["REGISTRY"]

schemas: dict = {}
cache = TemporaryDirectory()
APP.logger.info(f"Initialized new cache dir {cache.name}")


@APP.errorhandler(HTTPException)
def handle_exception(e):
    response = e.get_response()
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


@APP.route("/schemas", methods=["GET"], strict_slashes=False)
def read_schemas():
    __schemas = registry.get_all_schemas()
    return jsonify(__schemas)


@APP.route("/schemas/ids/<schema_id>", methods=["GET"])
def read_schema_id(schema_id: int):
    schema_id = int(schema_id)
    if schema_id in schemas:
        return jsonify(schemas[schema_id].schema)

    __schema = registry.get_schema_from_id_raw(int(schema_id))
    if __schema.status_code != 200:
        return __schema.json(), __schema.status_code
    schema = Schema(
        int(schema_id), definition=__schema.json(), cache_dir=cache, registry=registry
    )
    schemas[schema_id] = schema
    APP.logger.info(f"Pulled schema {schema_id} from source schema registry.")
    return jsonify(schemas[schema_id].schema)


@APP.route("/subjects", methods=["GET"], strict_slashes=False)
def read_subjects():
    return jsonify(registry.get_all_subjects())


@APP.route("/subjects/<subject_name>/versions", methods=["GET"], strict_slashes=False)
def read_subject_versions(subject_name: str):
    return jsonify(registry.get_subject_versions(subject_name))


@APP.route("/subjects/<subject_name>/versions/<version_id>/schema", methods=["GET"], strict_slashes=True)
def read_subject_versions_schema(subject_name: str, version_id: int):
    return jsonify(registry.get_subject_versions_schema(subject_name, version_id))


@APP.route("/healthcheck", methods=["GET"])
def healthcheck():

    return_code = 200
    health = {"upstream_access": True, "schemas": len(schemas.keys())}
    url = f"{registry.SchemaRegistryUrl}/config"
    APP.logger.debug(url)
    if not registry.Username:
        req = requests.get(url)
    else:
        req = requests.get(url, auth=(registry.Username, registry.Password))
    if req.status_code != 200:
        return_code = 500

    return health, return_code
