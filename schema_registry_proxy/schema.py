# SPDX-License-Identifier: Apache-2.0
# Copyright 2020-2022 John Mille <john@compose-x.io>
"""
Class to manage and represent a schema
"""
from tempfile import TemporaryDirectory
from compose_x_common.compose_x_common import keyisset
from kafka_schema_registry_admin.kafka_schema_registry_admin import SchemaRegistry
import json
from datetime import datetime as dt
from typing import Union


class Schema:
    def __init__(
        self,
        schema_id: int,
        definition: Union[str, dict],
        cache_dir: TemporaryDirectory = None,
        registry: SchemaRegistry = None,
    ):
        """
        Class to manage the schema and caching
        :param schema_id:
        """
        self.__id = schema_id
        self.cache_dir = TemporaryDirectory() if not cache_dir else cache_dir
        self.retrieved_at = None
        self.registry = registry
        self.definition = definition

    @property
    def schema_id(self) -> int:
        return int(self.__id)

    @property
    def file_name(self) -> str:
        return f"{self.cache_dir.name}/{self.schema_id}"

    @property
    def definition(self) -> dict:
        try:
            with open(self.file_name, "r") as def_fd:
                return json.loads(def_fd.read())
        except IOError:
            if self.registry:
                self.registry.get_schema_from_id_raw(self.schema_id)

    @definition.setter
    def definition(self, definition: Union[str, dict]):
        if not isinstance(definition, (str, dict)):
            raise TypeError(
                "definition must be a string or dict. got", definition, type(definition)
            )
        self.retrieved_at = dt.utcnow()
        __definition = (
            definition if isinstance(definition, dict) else json.loads(definition)
        )
        with open(self.file_name, "w") as schema_fd:
            schema_fd.write(json.dumps(__definition))

    @property
    def schema(self) -> dict:
        if not keyisset("schema", self.definition):
            return {"schema": json.dumps(self.definition)}
        return self.definition
