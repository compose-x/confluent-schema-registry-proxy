# SPDX-License-Identifier: Apache-2.0
# Copyright 2020-2022 John Mille <john@compose-x.io>

from os import environ, path
from kafka_schema_registry_admin.kafka_schema_registry_admin import SchemaRegistry
from .secret import Secret

_basedir = path.abspath(path.dirname(__file__))


class Config:
    DEBUG = False
    ENV = "production"
    if "LOGLEVEL" in environ and environ.get("LOGLEVEL").upper() == "DEBUG":
        DEBUG = True
    if DEBUG:
        ENV = "development"

    @property
    def REGISTRY(self):
        secret_arn = environ.get("SECRET_ARN", False)
        if not secret_arn:
            registry = SchemaRegistry(
                SchemaRegistryUrl=environ.get("SCHEMA_REGISTRY_URL")
            )
        else:

            secret = Secret(secret_arn)
            registry = SchemaRegistry(
                SchemaRegistryUrl=secret.secret_json["SCHEMA_REGISTRY_URL"],
                Username=secret.secret_json[
                    "SCHEMA_REGISTRY_BASIC_AUTH_USER_INFO"
                ].split(":")[0],
                Password=secret.secret_json[
                    "SCHEMA_REGISTRY_BASIC_AUTH_USER_INFO"
                ].split(":")[1],
            )
        return registry
