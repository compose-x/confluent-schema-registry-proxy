#!/usr/bin/env python
# SPDX-License-Identifier: Apache-2.0
# Copyright 2020-2022 John Mille <john@compose-x.io>

"""
Simple rest proxy pulling secrets from AWS secrets manager
"""

from schema_registry_proxy import APP

if __name__ == "__main__":
    APP.run()
